import Users from "../views/Users/Users";
import Tasks from "../views/Tasks/Tasks";

export const MENU_ITEMS = [
    {
        title: 'Пользователи',
        path: '/',
        component: Users
    },
    {
        title: 'Задачи',
        path: '/tasks',
        component: Tasks
    }
];