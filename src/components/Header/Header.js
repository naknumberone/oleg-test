import React from "react";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types'
import './Header.sass'

const Header = ({menuItems}) => {
    return (
        <div className="header__wrapper">
            {menuItems.map((item, index) =>
                <div className="menu_item" key={index}>
                    <Link to={item.path}>{item.title}</Link>
                </div>
            )}
        </div>
    );
};

Header.propTypes = {
  menuItems: PropTypes.arrayOf(
      PropTypes.shape({
          title: PropTypes.string,
          path: PropTypes.string
      })
  )
};

export default Header;