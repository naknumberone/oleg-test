import React, {useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import Header from "./components/Header/Header";
import {MENU_ITEMS} from "./constants/menu";
import {Switch, Route} from 'react-router-dom'
import Users from "./views/Users/Users";
import Tasks from "./views/Tasks/Tasks";

function App() {
  return (
    <div className="App">
     <Header menuItems={MENU_ITEMS} />
     <Switch>
         {/*{MENU_ITEMS.map(item => <Route exact to={item.path} component={item.component} />)}*/}
         <Route exact to={'/'} component={Users} />
         <Route exact to={'/tasks'} component={Tasks} />
     </Switch>
    </div>
  );
}

export default App;
